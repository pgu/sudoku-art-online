package ensemble is

   APPARTIENT_ENSEMBLE : exception;
   NON_APPARTIENT_ENSEMBLE : exception;

   type Type_Ensemble is private;

   -- retourne un ensemble vide
   function construireEnsemble return Type_Ensemble is
    i   : Integer;
    e   : Type_Ensemble;
   begin
   i:=1;
    while i <= 9 loop
      ens(i) := false;
      i:=i+1;
    end loop;
    return ens;
   end construireEnsemble;

   -- retourne VRAI si l'ensemble e est vide et FAUX sinon
   function ensembleVide (e : in Type_Ensemble) return Boolean is
    i   : Integer;
    sum : Boolean;
   begin
    i:=1;
    sum:=0;
    while i <= 9 loop
      sum:=sum or e(i);
      i:=i+1;
    end loop;
    return sum = 0;
   end ensembleVide;

   -- retourne VRAI si le chiffre v est present dans l'ensemble e
   function appartientChiffre
     (e : in Type_Ensemble; v : in Integer) return Boolean is
    begin
      return e(v);
    end appartientChiffre;

   -- retourne le nombre d'elements de l'ensemble e
   function nombreChiffres (e : in Type_Ensemble) return Integer is
    i   : Integer;
    len : integer;
   begin
    
    i:=1;
    len:=0;

    while i <= 9 loop

      -- si la case de l'ensemble n'est pas vide (=false)
      -- on ajoute 1 à la longueur effective de l'ensemble
      if e(i) then
        len:=len+1;
      end if;

      i:=i+1
    end loop;
   end nombreChiffres;

   -- ajoute le chiffre v dans l'ensemble e
   -- nécessite l'element v n'existe pas dans l'ensemble e
   -- lève l'exception APPARTIENT_ENSEMBLE si v appartient à l'ensemble
   procedure ajouterChiffre (e : in out Type_Ensemble; v : in Integer) is
   begin
    if not appartientChiffre(e, v) then
      e(v) := true;
    else
      raise APPARTIENT_ENSEMBLE;
    end if;
   end ajouterChiffre;

   -- supprime le chiffre v dans l'ensemble e
   -- nécessite l'element v existe dans l'ensemble e
   -- lève l'exception NON_APPARTIENT_ENSEMBLE si v n'appartient pas à l'ensemble
   procedure retirerChiffre (e : in out Type_Ensemble; v : in Integer) is
    i       : Integer;
    trouve  : Boolean;
    idx     : Interger;
   begin
    if appartientChiffre(e, v) then
      e(v) := false;
    else
      raise NON_APPARTIENT_ENSEMBLE;
    end if;
   end retirerChiffre;

private
   type Type_Ensemble is array (1 .. 9) of Boolean;

end ensemble;