package body grilleSudoku is

   ----------------------
   -- construireGrille --
   ----------------------

   function construireGrille return Type_Grille is
      grille : Type_Grille;      
   begin 
      for i in (1..9) loop
         for z in (1..9) loop
            grille(i,z):=0;                                       
         end loop;          loop;             
      end loop;
      return grille; 

   end construireGrille;

   --------------
   -- caseVide --
   --------------

   function caseVide (g: in Type_Grille; c: in Type_Coordonnee) return Boolean is
   begin   
      return not g(obtenirLigne(c))(obtenirColonne(c)) >= 1 and g(obtenirLigne(c))(obtenirColonne(c)) <= 9;      
   end caseVide;

   --------------------
   -- obtenirChiffre --
   --------------------

   function obtenirChiffre (g: in Type_Grille;  c: in Type_Coordonnee) return Integer is
   begin
      return g(obtenirLigne(c))(obtenirColonne(c));
   end obtenirChiffre;

   --------------------
   -- nombreChiffres --
   --------------------

   function nombreChiffres (g : in Type_Grille) return Integer is
   begin
      for i in (1..9) loop
                     
   end nombreChiffres;

   ------------------
   -- fixerChiffre --
   ------------------

   procedure fixerChiffre(g: in out Type_Grille; c: in Type_Coordonnee; v: in Integer) is
   begin

   end fixerChiffre;

   ---------------
   -- viderCase --
   ---------------

   procedure viderCase (g: in out Type_Grille; c: in out Type_Coordonnee) is
   begin

   end viderCase;

   ----------------
   -- estRemplie --
   ----------------

   function estRemplie (g: in Type_Grille) return Boolean is
   begin

   end estRemplie;

   ------------------------------
   -- obtenirChiffresDUneLigne --
   ------------------------------

   function obtenirChiffresDUneLigne (g: in Type_Grille; numLigne: in Integer) return Type_Ensemble is
   begin

   end obtenirChiffresDUneLigne;

   --------------------------------
   -- obtenirChiffresDUneColonne --
   --------------------------------

   function obtenirChiffresDUneColonne (g: in Type_Grille; numColonne: in Integer) return Type_Ensemble is
   begin

   end obtenirChiffresDUneColonne;

   -----------------------------
   -- obtenirChiffresDUnCarre --
   -----------------------------

   function obtenirChiffresDUnCarre (g: in Type_Grille; numCarre : in Integer) return Type_Ensemble is
   begin

   end obtenirChiffresDUnCarre;

end grilleSudoku;
