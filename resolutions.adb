package body resolutions is

   -----------------------
   -- estChiffreValable --
   -----------------------

   function estChiffreValable
     (g : in Type_Grille;
      v :    Integer;
      c :    Type_Coordonnee)
      return Boolean
   is
      ligne:Type_Ensemble;--ensemble contenant les chiffres de la ligne généré à partir des coordonnées c
      colonne: Type_Ensemble;--ensemble contenant les chiffres de la colonne généré à partir des coordonnées c
      carre:Type_Ensemble;--ensemble contenant les chiffres du carre généré à partir des coordonnées c
   begin
      if not casevide(g,c) then
         raise CASE_NON_VIDE;
      end if;
      --on génère les ensembles correspondant à la ligne, à la colonne et au carre de coordonnées c
      ligne:=obtenirChiffresDUneLigne(g, obtenirLigne(c));
      colonne:=obtenirChiffresDUneColonne(g, obtenirColonne(c));
      carre:=obtenirChiffresDUnCarre(g, obtenirCarre(c));
      --on vérifie si la valkeur testée appartient à un des ensembles
      if appartientChiffre(ligne, v) or appartientChiffre(colonne, v) or appartientChiffre(carre, v) then
         return False;
      else
         return True;
      end if;
   end estChiffreValable;

   -------------------------------
   -- obtenirSolutionsPossibles --
   -------------------------------

   function obtenirSolutionsPossibles
     (g : in Type_Grille;
      c : in Type_Coordonnee)
      return Type_Ensemble
   is
   begin


   end obtenirSolutionsPossibles;

   ------------------------------------------
   -- rechercherSolutionUniqueDansEnsemble --
   ------------------------------------------

   function rechercherSolutionUniqueDansEnsemble
     (resultats : in Type_Ensemble)
      return Integer
   is
   begin

   end rechercherSolutionUniqueDansEnsemble;

   --------------------
   -- resoudreSudoku --
   --------------------

   procedure resoudreSudoku
     (g      : in out Type_Grille;
      trouve :    out Boolean)
   is
   begin

   end resoudreSudoku;

end resolutions;
