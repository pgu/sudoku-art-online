package body Coordonnee is

   ---------------------------
   -- construireCoordonnees --
   ---------------------------

   function construireCoordonnees (ligne: Integer; colonne: Integer) return Type_Coordonnee is
      coord : Type_Coordonnee;      
   begin
      coord.ligne:=ligne;
      coord.colonne:=colonne;
      return coord;      
   end construireCoordonnees;

   ------------------
   -- obtenirLigne --
   ------------------

   function obtenirLigne (c : Type_Coordonnee) return Integer is
   begin
      return c.ligne;
   end obtenirLigne;

   --------------------
   -- obtenirColonne --
   --------------------

   function obtenirColonne (c : Type_Coordonnee) return Integer is
   begin
      return c.colonne;
   end obtenirColonne;

   ------------------
   -- obtenirCarre --
   ------------------

   function obtenirCarre (c : Type_Coordonnee) return Integer is
   begin
      if c.ligne <= 3 then
         if c.colonne <= 3 then
            return 1;
         elsif c.colonne <= 6 then
            return 2;
         elsif c.colonne <= 9 then
            return 3;
         end if;
      elsif c.ligne <= 6 then
         if c.colonne <= 3 then
            return 4;
         elsif c.colonne <= 6 then
            return 5;
         elsif c.colonne <= 9 then
            return 6;
         end if;
      elsif c.ligne <= 9 then
         if c.colonne <= 3 then
            return 7;
         elsif c.colonne <= 6 then
            return 8;
         elsif c.colonne <= 9 then
            return 9;
         end if;
      end if;                                 
   end obtenirCarre;

   ----------------------------
   -- obtenirCoordonneeCarre --
   ----------------------------

   function obtenirCoordonneeCarre (numCarre : Integer) return Type_Coordonnee is
      c: Type_Coordonnee;
   begin
      case numCarre is
         when 1 => c.ligne := 1; c.colonne := 1;
         when 2 => c.ligne := 1; c.colonne := 4;
         when 3 => c.ligne := 1; c.colonne := 7;
         when 4 => c.ligne := 4; c.colonne := 1;
         when 5 => c.ligne := 4; c.colonne := 4;
         when 6 => c.ligne := 4; c.colonne := 7;
         when 7 => c.ligne := 7; c.colonne := 1;
         when 8 => c.ligne := 7; c.colonne := 4;
         when 9 => c.ligne := 7; c.colonne := 7;
         when others => raise TRANCHE_COOR_INVALIDE;
      end case;
      return c;

   end obtenirCoordonneeCarre;

end Coordonnee;
